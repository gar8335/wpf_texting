﻿/**
 *  Gretchen Raffensberger
 *  Programming Skills C#
 *  Lab 2
 *  9/27/2013
 *  
 * This is the controller -- it interfaces between the view and the model
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhoneSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PhoneWindow : Window
    {
		private PhoneModel model;

		public PhoneWindow()
		{
			model = new PhoneModel();
            InitializeComponent();
		}

		/// <summary>
		/// Updates model on click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void Number_Click(object sender, RoutedEventArgs e)
        {
			Button clickedNumber = sender as Button;

			string newTextForWordsEntered = WordsEntered.Text;
			switch (clickedNumber.Name)
			{
				case "Two":
					newTextForWordsEntered = model.updateNumberClicked(2);
					break;
				case "Three":
					newTextForWordsEntered = model.updateNumberClicked(3);
					break;
				case "Four":
					newTextForWordsEntered = model.updateNumberClicked(4);
					break;
				case "Five":
					newTextForWordsEntered = model.updateNumberClicked(5);
					break;
				case "Six":
					newTextForWordsEntered = model.updateNumberClicked(6);
					break;
				case "Seven":
					newTextForWordsEntered = model.updateNumberClicked(7);
					break;
				case "Eight":
					newTextForWordsEntered = model.updateNumberClicked(8);
					break;
				case "Nine":
					newTextForWordsEntered = model.updateNumberClicked(9);
					break;
				default:
					throw new ArgumentException("I'm sorry, I've messed up horrifically! Have a lovely day.");
			}
            WordsEntered.Text = newTextForWordsEntered;
        }

		/// <summary>
		///  Updates model for space click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Space_Click(object sender, RoutedEventArgs e)
		{
			WordsEntered.Text = model.updateSpaceClicked();
		}

		/// <summary>
		/// Updates model for backspace
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Backspace_Click(object sender, RoutedEventArgs e)
		{
			WordsEntered.Text = model.updateBackspaceClicked();
		}

		/// <summary>
		///  Updates model when predictive is checked or unchecked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Predictive_Checked(object sender, RoutedEventArgs e)
		{
			model.updatePredictiveMode();
		}

		/// <summary>
		///  Updates model when next is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Next_Click(object sender, RoutedEventArgs e)
		{
			WordsEntered.Text = model.updateNextClicked();
		}

        

    }
}
