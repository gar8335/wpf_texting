﻿/**
 *  Gretchen Raffensberger
 *  Programming Skills C#
 *  Lab 2
 *  9/27/2013
 *  
 *  This is the model part of MVC-- all of the data is stored here.
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneSimulator
{
	public class PhoneModel
	{
		/// <summary>
		/// GENERAL variables
		/// </summary>
		private const string DICTIONARY_FILENAME = "english-words.txt";
		// stores the text that will go in the textbox
		// (minus any "in progress" predictive word)
		private string typedText;		
		private bool predictiveMode;		// true if predictive mode is on

		/// <summary>
		/// NON-PREDICTIVE MODE variables
		/// </summary>
		private int numberOfTimesClicked;	// how many times was this button clicked
		private int lastNumberClicked;		// what was the last number button clicked
		private DateTime lastTimePressed;	// what was the last time this was pressed

		/// <summary>
		/// PREDICTIVE MODE variables
		/// </summary>
		private List<string> possibleWords;	// possible words given current key presses
		private string keysPressed;			// keys pressed since last space
		private int indexToPossible;		// index into list of possible words
		private string currentPredictedWord;// current word predicted, out of possibleWords list

		// for each number on the phone, stores how many letters you can cycle through
		// example: pressing 4 can give you D, E, F, so 4 maps to 3
		private Dictionary<int, int> maxClickForNumber;

		// stores the letters associated with each number, like 2 goes to a,b,c
		private Dictionary<int, List<string>> numberToLetter;

		// stores mapping from letter as char to number as string, like 'a' --> "2"
		private Dictionary<char, string> letterToNumberString;

		// stores all the words from given dictionary
		private Dictionary<string, List<string>> dictionaryOfWords;

		public PhoneModel()
		{
			numberOfTimesClicked = 0;
			lastNumberClicked = 0;
			lastTimePressed = DateTime.Now;
            typedText = "";
			predictiveMode = false;
			possibleWords = new List<string>();
			keysPressed = "";
			indexToPossible = -1;
			currentPredictedWord = "";

			maxClickForNumber = new Dictionary<int, int>
				{ {2,3},{3,3},{4,3},{5,3},{6,3},{7,4},{8,3},{9,4} };

			numberToLetter = new Dictionary<int, List<string>>
				{
					{2, new List<String> {"a", "b", "c" } },
					{3, new List<String> {"d", "e", "f" } },
					{4, new List<String> {"g", "h", "i" } },
					{5, new List<String> {"j", "k", "l" } },
					{6, new List<String> {"m", "n", "o" } },
					{7, new List<String> {"p", "q", "r", "s" } },
					{8, new List<String> {"t", "u", "v" } },
					{9, new List<String> {"w", "x", "y", "z" } }
				};

			letterToNumberString = new Dictionary<char, string>
			{
				{'a', "2"}, {'b', "2"}, {'c', "2"},
				{'d', "3"}, {'e', "3"}, {'f', "3"},
				{'g', "4"}, {'h', "4"}, {'i', "4"}, 
				{'j', "5"}, {'k', "5"}, {'l', "5"}, 
				{'m', "6"}, {'n', "6"}, {'o', "6"},
				{'p', "7"}, {'q', "7"}, {'r', "7"}, {'s', "7"}, 
				{'t', "8"}, {'u', "8"}, {'v', "8"}, 
				{'w', "9"}, {'x', "9"}, {'y', "9"}, {'z', "9"}
			};

			dictionaryOfWords = new Dictionary<string, List<string>>();

			parseWordFile(DICTIONARY_FILENAME);

		}

		/// <summary>
		/// Parses all the words from dictionary and puts them into dictionary structures
		/// Maps key presses (as string) to list of words
		/// </summary>
		/// <param name="DICTIONARY_FILENAME"></param>
		private void parseWordFile(string DICTIONARY_FILENAME)
		{
			string word;
			StreamReader wordFileReader = new StreamReader(File.OpenRead(DICTIONARY_FILENAME));

			try
			{
				while ((word = wordFileReader.ReadLine()) != null)
				{
					word = word.Trim();
					string numbersThatMakeUpWord = "";
					foreach( char c in word )
					{
						numbersThatMakeUpWord += letterToNumberString[c];
					}
					// do all the checks to make sure that you don't get any stupid errors!!!!!
					if (!dictionaryOfWords.ContainsKey(numbersThatMakeUpWord) || dictionaryOfWords[numbersThatMakeUpWord] == null)
					{
						dictionaryOfWords.Add(numbersThatMakeUpWord, new List<string>());
					}
					dictionaryOfWords[numbersThatMakeUpWord].Add(word);
				}
				
			}
			catch (IOException e)
			{
				Console.Error.WriteLine("Error in reading word file: " + e.Message);
				throw e;
			}
			catch (OutOfMemoryException ex)
			{
				Console.Error.WriteLine("Not enough memory to read word file: " + ex.Message);
				throw ex;
			}
			
			
		}

		/// <summary>
		/// This updates the model based on the number clicked and state of predicted mode
		/// </summary>
		/// <param name="numberClicked">the number pressed on the phone</param>
		/// <returns>string to update in text field</returns>
		public string updateNumberClicked(int numberClicked)
		{
			if (!predictiveMode)
			{
				updateNumberClickedNonPredictive(numberClicked);
			}
			else
			{
				updateNumberClickedPredictive(numberClicked);
				return typedText + currentPredictedWord;
			}
            return typedText;
		}

		/// <summary>
		/// updates for non predictive mode
		/// keeps track of if a new number was pressed (or timed out), 
		/// or if it should cycle through the 
		/// </summary>
		/// <param name="numberClicked"></param>
		private void updateNumberClickedNonPredictive(int numberClicked)
		{
			bool removeCharacterFromString = true;
			DateTime timePressed = DateTime.Now;
			string letterPressed = "";
			if (numberClicked == lastNumberClicked      // if number was clicked repetitively
				&&                                      // and within a short time of last press
				timePressed.CompareTo(lastTimePressed.AddSeconds(1)) < 0)
			{
				// wrap around if you hit it 3 or 4 times in a row
				numberOfTimesClicked = (numberOfTimesClicked + 1)
					% maxClickForNumber[numberClicked];
			}
			else
			{			// start afresh for new button
				lastNumberClicked = numberClicked;
				numberOfTimesClicked = 0;
				removeCharacterFromString = false;
			}
			lastTimePressed = timePressed;
			letterPressed = numberToLetter[lastNumberClicked].ElementAt(numberOfTimesClicked);
			// update string for text field
			if (removeCharacterFromString)
			{
				typedText = typedText.Remove(typedText.Length - 1);
			}
			typedText += letterPressed;
		}

		/// <summary>
		/// Updates when in predictive mode, based on number pressed
		/// recalculates when a new word is predicted
		/// </summary>
		/// <param name="numberClicked"></param>
		private void updateNumberClickedPredictive(int numberClicked)
		{
			keysPressed += numberClicked.ToString();
			recalculatePredictedWord();
		}

		/// <summary>
		/// Using current key presses, looks up what possible words can be made from them
		/// If no word can be made, it will indicate it with ---- 
		/// </summary>
		private void recalculatePredictedWord()
		{
			if (dictionaryOfWords.ContainsKey(keysPressed))
			{
				possibleWords = new List< string >(dictionaryOfWords[keysPressed]);
				currentPredictedWord = possibleWords.ElementAt(0);
				indexToPossible = 0;
			}
			else
			{
				currentPredictedWord =  String.Concat(Enumerable.Repeat("-", keysPressed.Length));
				indexToPossible = -1;
				possibleWords.Clear();
			}
		}
	
		/// <summary>
		/// Clears out all information
		/// Useful for things like switching mode, spaces, etc.
		/// </summary>
		private void resetClickInformation()
		{
			numberOfTimesClicked = 0;
			lastNumberClicked = 0;
			lastTimePressed = DateTime.Now;
			keysPressed = "";
			currentPredictedWord = null;
			possibleWords.Clear();
			indexToPossible = -1;
			
		}

		/// <summary>
		/// Puts in a space
		/// </summary>
		/// <returns> updated string for text field</returns>
		public string updateSpaceClicked()
		{
			if (predictiveMode && currentPredictedWord != null)
			{
				typedText += currentPredictedWord;
			}
			typedText += " ";
			resetClickInformation();
			return typedText;
		}

		/// <summary>
		/// Long and twisty logic
		/// If predictive and ending in a single space, remove whole last word
		/// Otherwise, remove the last character typed
		/// </summary>
		/// <returns></returns>
		public string updateBackspaceClicked()
		{
			if (predictiveMode)
			{
				if (keysPressed.Length > 0) 
				{
					keysPressed = keysPressed.Remove(keysPressed.Length - 1);
					recalculatePredictedWord();
					return typedText + currentPredictedWord;
				}
				else if (typedText.Length > 0)
				{
					if (typedText.ElementAt(typedText.Length - 1) == ' ')
					{
						typedText = typedText.Remove(typedText.Length - 1);	// remove space at end
						modifyText("");
						resetClickInformation();
					}
					else
					{
						typedText = typedText.Remove(typedText.Length - 1);
					}
				}
			}
			else if (typedText.Length > 0) 
			{
				typedText = typedText.Remove(typedText.Length - 1);
			}
			return typedText;
		}

		/// <summary>
		/// Replaces the last word in typed text (delimited with spaces) with the given new word
		/// Effectively removes last word in typed text if given is empty string
		/// </summary>
		/// <param name="newWord"> new word to be inserted</param>
		/// PRECONDITION: typedText cannot end in space!!!!!!!!!
		/// If it does end in a space, the space will be removed and nothing else
		private void modifyText(string newWord)
		{
			if (typedText.Length > 0 && typedText.ElementAt( typedText.Length - 1) == ' ')
			{
				typedText.Remove(typedText.Length - 1);
				return;
			}
			int beginningIndexOfLastWord = typedText.LastIndexOf(' ') + 1;
			if (typedText != "")
			{
				typedText = typedText.Remove(beginningIndexOfLastWord);
			}
			typedText += newWord;
		}

		/// <summary>
		/// Toggles boolean for predictive mode
		/// Clears out all necessary state
		/// </summary>
		public void updatePredictiveMode()
		{
			if (predictiveMode && currentPredictedWord != null)
			{
				typedText += currentPredictedWord;
			}
			predictiveMode = !predictiveMode;
			resetClickInformation();
		}

		/// <summary>
		/// If possible, goes to next possible word in predictive mode
		/// </summary>
		/// <returns>updates string for text field</returns>
		public string updateNextClicked()
		{
			if (predictiveMode)
			{

				if (indexToPossible >= 0)
				{
					indexToPossible = (indexToPossible + 1) % possibleWords.Count;
					currentPredictedWord = possibleWords[indexToPossible];
				}
				if (currentPredictedWord != null)
				{
					return typedText + currentPredictedWord;
				}
			}
			return typedText;
		}
	}

}
